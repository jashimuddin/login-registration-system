<?php 
// require_once 'lib/Session.php';
require_once 'lib/User.php';
require_once 'inc/header.php'; 
Session::checkSession();

if(isset($_GET['id'])){
	$id = $_GET['id'];
}
$data = new User();
$result = $data->getUserByID($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])){
	$updateUser = $data->updateUserData($id, $_POST);
}
$msg = Session::get("msg");

 if (isset($msg)){
     echo $msg;
 }

 Session::set('msg', NULL);
 
if($result){

?>
  <!--  Breadcrumb -->
  <div class="container">
      <div class="breadcrumb">
          <a href="index.php">Home</a>
          <a href="profile.php" class="active">Profile</a>
      </div>
  </div>
  <!--  // Breadcrumb -->
  
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Update User Profile</h3>
				</div >
				<?php 
					if(isset($updateUser)){
						echo $updateUser;
					}
				?>
				<div class="panel-body">
					<form action="profile.php" method="post">
						<div class="form-group">
							<label for="">Your Name:</label>
							<input type="text" class="form-control" name="name" value="<?php echo $result->name; ?>" >
						</div>
						<div class="form-group">
							<label for="">Username:</label>
							<input type="text" class="form-control" name="username" value="<?php echo $result->username; ?>">
						</div>
						<div class="form-group">
							<label for="">Email:</label>
							<input type="text" class="form-control" name="email" value="<?php echo $result->email; ?>">
						</div>
						<?php 
							$sesID = Session::get('id');
							if($sesID == $id){
						?>
						<button class="btn btn-primary pull-right" type="submit" name="update"> Update </button>
						<a href="changepass.php?id=<?php echo $id; ?>" class="btn btn-primary">Change Passowrd </a>       

							<?php } ?>
					</form>
<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>  <!-- // login container ended here -->





<?php  require_once 'inc/footer.php'; ?>